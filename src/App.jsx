import React, { useState } from "react";
import { Container, Header, RefreshRecycle, Buttons } from "./AllComponents";
import "./index.css";

function App() {
  const [elements, setElements] = useState([
    { count: 0, id: 0 },
    { count: 0, id: 1 },
    { count: 0, id: 2 },
    { count: 0, id: 3 },
  ]);

  return (
    <div className="main">
      <Container>
        <Header elements={elements} />
        <RefreshRecycle elements={elements} setElements={setElements} />
        <Buttons elements={elements} setElements={setElements} />
      </Container>
    </div>
  );
}

export default App;
