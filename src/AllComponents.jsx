const Container = (props) => {
  return <div className="container">{props.children}</div>;
};

const Header = ({ elements }) => {
  return (
    <div className="header">
      <div className="cart">
        <img className="cartImg" src="/Cart.png" />
      </div>
      <div className="overallCount">
        {elements.reduce((acc, item) => {
          if (item.count > 0) acc += 1;
          return acc;
        }, 0)}
      </div>
      <div className="items">Items</div>
    </div>
  );
};

const RefreshRecycle = ({ elements, setElements }) => {
  const handleRefresh = () => {
    const newArr = elements.map((item) => {
      return { count: 0, id: item.id };
    });
    setElements(newArr);
  };

  const handleRecycle = () => {
    if (elements.length === 0) {
      setElements([
        { count: 0, id: 0 },
        { count: 0, id: 1 },
        { count: 0, id: 2 },
        { count: 0, id: 3 },
      ]);
    }
  };

  return (
    <div className="RefreshRecycle">
      <div className="refresh" onClick={handleRefresh}>
        <img className="refreshImg" src="/Refresh.png" />
      </div>
      <div className="recycle" onClick={handleRecycle}>
        <img className="recycleImg" src="/Recycle.png" />
      </div>
    </div>
  );
};

const Buttons = ({ elements, setElements }) => {
  const handleIncrement = (id) => {
    const newArr = elements.map((item) => {
      if (item.id === id) {
        item.count += 1;
      }
      return item;
    });
    setElements(newArr);
  };

  const handleDecrement = (id) => {
    const newArr = elements.map((item) => {
      if (item.id === id) {
        if (item.count > 0) {
          item.count -= 1;
        }
      }
      return item;
    });
    setElements(newArr);
  };

  const handleRemoveClick = (id) => {
    const newArr = elements.filter((item) => item.id !== id);
    setElements(newArr);
  };

  return (
    <>
      {elements.map((item) => {
        return (
          <div className="buttons" id={item.id} key={item.id}>
            <div className={item.count ? " zero afterIncrement" : "zero"}>
              {item.count === 0 ? "zero" : item.count}
            </div>
            <div className="increment" onClick={() => handleIncrement(item.id)}>
              +
            </div>
            <div className="decrement" onClick={() => handleDecrement(item.id)}>
              -
            </div>
            <div className="remove" onClick={() => handleRemoveClick(item.id)}>
              <img src="/Remove.svg" />
            </div>
          </div>
        );
      })}
    </>
  );
};

export { Container, Header, RefreshRecycle, Buttons };
